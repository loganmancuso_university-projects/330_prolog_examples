% All edges are assumed to leave the vertex in question.

% arrow(left,bottom,right)
arrow('+','-','+').  arrow('-','+','-').  arrow('<','+','>').

% fork(left,top,right)
fork('+','+','+').  fork('-','-','-').  fork('>','-','<').
fork('-','<','>').  fork('<','>','-').   

% tvertex(left,bottom,right)
tvertex('>',_,'<').

% lvertex(left,right)
lvertex('<','>').  lvertex('<','-').   lvertex('>','<').     
lvertex('>','+').  lvertex('-','>').   lvertex('+','<').

% Reversing the direction of an edge: only < and > change.
reverse('+','+').        reverse('-','-').
reverse('<','>').        reverse('>','<').
