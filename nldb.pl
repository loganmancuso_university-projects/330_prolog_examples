person(john). person(george). person(mary). person(linda).  
park(kew_beach). park(queens_park). 
tree(tree01). tree(tree02).  tree(tree03).  
hat(hat01).   hat(hat02).  hat(hat03).  hat(hat04).

sex(john,male).    sex(george,male). 
sex(mary,female).  sex(linda,female).

color(hat01,red).   color(hat02,blue). 
color(hat03,red).   color(hat04,blue).  

in(john,kew_beach).     in(george,kew_beach). 
in(linda,queens_park).  in(mary,queens_park).  
in(tree01,queens_park). in(tree02,queens_park). 
in(tree03,kew_beach).

beside(mary,linda). beside(linda,mary). 

on(hat01,john). on(hat02,mary). on(hat03,linda). on(hat04,george). 

size(john,small).    size(george,big). 
size(mary,small).    size(linda,small). 
size(hat01,small).   size(hat02,small). 
size(hat03,big).     size(hat04,big).  
size(tree01,big).    size(tree02,small).  size(tree03,small). 
