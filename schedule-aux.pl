% Monday: only 11am and 1pm are available.
taken(X) :- X // 100 =:= 1, \+ X=111, \+ X=113. 
% Tuesday: only 1pm and 2pm are available.
taken(X) :- X // 100 =:= 2, \+ X=213, \+ X=214. 
% Wednesday: nothing available.
taken(X) :- X // 100 =:= 3. 
% Thursday: all available, except for 10am, 12pm, and 2pm.
taken(410). taken(412). taken(414). 
% Friday: nothing available.
taken(X) :- X // 100 =:= 5.
