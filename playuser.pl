% play_user(U): play entire game, getting moves for U from terminal.
play_user(U) :- 
   initial_state(S,P), write('The first player is '), write(P), 
   write(' and the initial state is '), write_state(S), 
   play_from(S,P,U).

% play_from(S,P,U): player P plays from state S with user U.
play_from(S,P,_) :-                       % Is the game over?
   game_over(S,P,W), write('-------- The winner is '), write(W).
play_from(S,P,U) :-                       % Continue with next move.
   opp(P,Q), get_move(S,P,M,U), legal_move(S,P,M,New), 
   write('Player '), write(P), write(' chooses move '), write(M),
   write(' and the new state is '), write_state(New), 
   play_from(New,Q,U).

write_state(S) :- nl, write('    '), write(S), nl.

% Get the next move either from the user or from gameplayer.pl.
get_move(S,P,M,U) :- \+ P=U, win_move(S,P,M).       % Try to win.
get_move(S,P,M,U) :- \+ P=U, tie_move(S,P,M).       % Try to tie.
get_move(S,P,M,U) :- \+ P=U, legal_move(S,P,M,_).   % Do anything.
get_move(_,P,M,P) :- 
   write('Enter user move (then a period): '), read(M).
