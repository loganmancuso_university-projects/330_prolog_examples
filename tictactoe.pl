player(x). player(o).              %  This is the tic-tac-toe game.

initial_state([-,-,-,-,-,-,-,-,-],x).              % x moves first.

game_over(S,_,Q) :- three_in_row(S,Q).             % A winner
game_over(S,_,neither) :- \+ legal_move(S,_,_,_).  % A tie

three_in_row([P,P,P,_,_,_,_,_,_],P) :- player(P). 
three_in_row([_,_,_,P,P,P,_,_,_],P) :- player(P). 
three_in_row([_,_,_,_,_,_,P,P,P],P) :- player(P). 
three_in_row([P,_,_,P,_,_,P,_,_],P) :- player(P). 
three_in_row([_,P,_,_,P,_,_,P,_],P) :- player(P). 
three_in_row([_,_,P,_,_,P,_,_,P],P) :- player(P). 
three_in_row([P,_,_,_,P,_,_,_,P],P) :- player(P). 
three_in_row([_,_,P,_,P,_,P,_,_],P) :- player(P).

legal_move([-,B,C,D,E,F,G,H,I],P,1,[P,B,C,D,E,F,G,H,I]).
legal_move([A,-,C,D,E,F,G,H,I],P,2,[A,P,C,D,E,F,G,H,I]).
legal_move([A,B,-,D,E,F,G,H,I],P,3,[A,B,P,D,E,F,G,H,I]).
legal_move([A,B,C,-,E,F,G,H,I],P,4,[A,B,C,P,E,F,G,H,I]).
legal_move([A,B,C,D,-,F,G,H,I],P,5,[A,B,C,D,P,F,G,H,I]).
legal_move([A,B,C,D,E,-,G,H,I],P,6,[A,B,C,D,E,P,G,H,I]).
legal_move([A,B,C,D,E,F,-,H,I],P,7,[A,B,C,D,E,F,P,H,I]).
legal_move([A,B,C,D,E,F,G,-,I],P,8,[A,B,C,D,E,F,G,P,I]).
legal_move([A,B,C,D,E,F,G,H,-],P,9,[A,B,C,D,E,F,G,H,P]).
